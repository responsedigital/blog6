<!-- Start Blog 6 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Blog posts in 2-4 columns of image, title, meta, and description. -->
@endif
<div class="blog-6"  is="fir-blog-6">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="blog-6__wrap {{ $classes }}">
      @for($x = 0; $x < $num_columns; $x++)
        <div class="blog-6__post">
          <a href="#">
            <img class="blog-6__post-img" src="{{ $post['featured_image'] ?: 'https://res.cloudinary.com/response-mktg/image/upload/q_auto,f_auto/v1594221943/fir/demos/img-placeholder.jpg' }}" alt="Placeholder">
            <h4 class="blog-6__post-title">{{ $post['title'] ?: $faker->text($maxNbChars = 20) }}</h4>
          </a>
          <span class="blog-6__post-meta">
            <p class="blog-6__post-author">{{ $post['author'] ?: $faker->name }}</p>
            <p class="blog-6__post-date">{{ $post['date'] ?: $faker->dayOfMonth($max = 'now') . ' ' . $faker->monthName($max = 'now')  . ' ' . $faker->year($max = 'now') }}</p>
          </span>
          <p class="blog-6__post-description">{{ $post['content'] ?: $faker->paragraph($nbSentences = 2, $variableNbSentences = true) }}</p>
        </div>
      @endfor
  </div>
</div>
<!-- End Blog 6 -->
