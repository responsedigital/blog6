module.exports = {
    'name'  : 'Blog 6',
    'camel' : 'Blog6',
    'slug'  : 'blog-6',
    'dob'   : 'Blog_6_1440',
    'desc'  : 'Blog posts in 2-4 columns of image, title, meta, and description.',
}